

# syntax=docker/dockerfile:1

FROM --platform=$BUILDPLATFORM golang:alpine AS build
WORKDIR /src

RUN apk add -U tzdata
RUN apk --update add ca-certificates

COPY . .
# COPY go.mod go.sum .
RUN go mod download
RUN go mod verify

ARG TARGETOS TARGETARCH
RUN --mount=target=. \
    --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
    GOOS=$TARGETOS GOARCH=$TARGETARCH CGO_ENABLED=0 go build -o /server ./cmd/add


FROM scratch

COPY --from=build /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /etc/group /etc/group

COPY --from=build /server .

EXPOSE 3000
CMD ["/server"]
