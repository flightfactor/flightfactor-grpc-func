# How to develop Go gRPC microservices and deploy in Kubernetes

## Related Articles
* [How to develop Go gRPC microservices and deploy in Kubernetes](https://medium.com/@shuza.sa/how-to-develop-go-grpc-microservices-and-deploy-in-kubernates-5eace0425bf8)
* [Debugging Go application inside Kubernetes from IDE
](https://medium.com/@shuza.sa/debugging-go-application-inside-kubernetes-from-ide-7e63b013c46e)


## WORKFLOW (dev)
```sh
* cd pkg/pd
* protoc -I . --go_out=plugins=grpc:. ./*.proto
* cd -

* docker buildx use $(docker buildx create --platform linux/amd64,linux/arm64)  
* docker buildx build -t "remotejob/summation-service:latest" --platform linux/amd64,linux/arm64 --push .
* docker buildx build -t "registry.gitlab.com/flightfactor/flightfactor-grpc-func:0.0.0" --platform linux/amd64,linux/arm64 --push .




* kubectl apply -f k8s/deployment/summation-service.yaml
* kubectl delete -f k8s/deployment/summation-service.yaml
* minikube service api-service --url
```
## DEPLOYMENT
```sh

* export KUBECONFIG=~/.kube_prod/config
* kubectl apply -f k8s/deployment/summation-service.yaml
* kubectl delete -f k8s/deployment/summation-service.yaml
* curl https://rollersoft.ml/gprc/api/10/11

 ```
